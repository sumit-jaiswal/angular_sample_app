(function() {
  'use strict';

  angular
  .module('device_app')
  .controller('DeviceController', DeviceController);

  DeviceController.$inject = ['Api', 'apiUrl', '$stateParams', '$state'];

  function DeviceController(Api, apiUrl, $stateParams, $state) {
    var vm = this;
    vm.formData = {};
    vm.results = {};

    vm.deviceList = function(){
      Api.callApi(apiUrl+'/api/devices', 'GET').then(function(result){
        vm.results = result.data.devices;
      });
    }

    vm.deviceAdd = function(){
      console.log(vm.formData);
      Api.add(apiUrl+'/api/add', 'POST', vm.formData).then(function(result){
        console.log(result);
       $state.go('home');
      });
    }

    vm.deviceDetails = function(){
      var deviceId = $stateParams.id;
      Api.callApi(apiUrl+deviceId, 'GET').then(function(result){
        vm.result = result.data.device;
      });
    }

    vm.deviceDelelte = function(id){
      alert('Delelted')
      Api.callApi(apiUrl+'id', 'DELETE').then(function(result){
        
      });
    }

    vm.deviceUpdate = function(){
      Api.callApi(apiUrl+'1', 'PUT',post_data).then(function(result){
      $state.go('home');        
      });
    }
  };
})();
