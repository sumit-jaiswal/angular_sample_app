(function() {
  'use strict';

  angular.module('device_app', [
    'ui.router'
    ])
  .constant('apiUrl', 'http://24fitness.co.in')
  .config(config);


  config.$inject = [ '$urlRouterProvider', '$stateProvider', '$locationProvider' ];
  function config($urlProvider, $stateProvider, $locationProvider) {
    $locationProvider.hashPrefix('!');

    $locationProvider.html5Mode({
      enabled: false,
      requireBase: false
    });

    $urlProvider.otherwise('/');

    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'templates/devices/device_list.html'
    })
    .state('deviceView', {
      url: '/device/view/:id',
      templateUrl: 'templates/devices/device_detail.html'
    })
    .state('deviceAdd', {
      url: '/device/add',
      templateUrl: 'templates/devices/device_add.html'
    })
    .state('deviceUpdate', {
      url: '/device/update/:id',
      templateUrl: 'templates/devices/device_update.html'
    });;


  } 
})();