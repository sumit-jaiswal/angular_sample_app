(function() {
  'use strict';

  angular
  .module('device_app')
  .factory('Api', Api);

  Api.$inject = ['$q', '$http'];

  function Api($q, $http) {
    return{
      callApi: function (pathname, method) {
      // console.log(pathname,,method);
      var deferred = $q.defer();
      $http({
        method: method,
        url: pathname,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: function (obj) {
          var str = [];
        for (var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
        },
        }).then(function successCallback(response) {
          deferred.resolve(response);
        },function errorCallback(err) {
          deferred.reject(err);
        });
            return deferred.promise;
        },
       add: function (pathname, method, post_data) {
      // console.log(pathname,,method);
      var deferred = $q.defer();
      $http({
        method: method,
        url: pathname,
        data : post_data,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: function (obj) {
          var str = [];
        for (var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
        },
        }).then(function successCallback(response) {
          deferred.resolve(response);
        },function errorCallback(err) {
          deferred.reject(err);
        });
            return deferred.promise;
        }
    }
  }
})();
